.ONESHELL: # Only applies to all target

VERSION = 1.0.0
ARGS ?= ""


build:
		@ansible-galaxy collection build

publish:
		@ansible-galaxy collection publish ./keltio-kubernetes-1.0.0.tar.gz --api-key $(?ARGS)
